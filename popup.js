const btn = document.getElementById("aws-button")
btn.addEventListener('click', () => loadAwsPage())

const urlBuilders = {
    "iam": buildIamUrl,
    // "lambda": buildLambdaUrl,
    // "log-group": buildLogGroupUrl,
    // "cloudformation": buildCloudFormationUrl
}
function loadAwsPage() {
    const arnField = document.getElementById("arn")
    const arnString = arnField.value
    const arnParts = arnString.split(':')

    const [
        arn,
        aws,
        service,
        region,
        account,
        ...id
    ] = arnParts

    const arnDetails = {
        service,
        region,
        account,
        id: id.join(':')        
    }

    const builder = urlBuilders[service]
    const url = builder(arnDetails)

    window.open(url)
}

function buildIamUrl(arn) {
    const afterSlash = arn.id.split('/')[1]
    return `https://console.aws.amazon.com/iam/home#/roles/${afterSlash}`
}